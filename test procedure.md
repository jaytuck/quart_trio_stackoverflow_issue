Test procedure:

* (terminal1) run hypercorn
```
(triows) ~/g/triows (master)> hypercorn --worker-class trio --bind 127.0.0.1:8080 --reload server
[2021-07-31 09:46:53 +0930] [57362] [INFO] Running on http://127.0.0.1:8080 (CTRL + C to quit)
```

* (terminal2) run python client.py cl1
```
(triows) ~/g/triows (master)> python client.py cl1
* cl1 joined
<cl1> hello
```

* (terminal3) run python client.py cl2
```
(triows) ~/g/triows[SIGINT]> python client.py cl2
* cl2 joined
<cl2> hello
```

* (terminal2) see that cl2 has joined
```
(triows) ~/g/triows (master)> python client.py cl1
* cl1 joined
<cl1> hello
* cl2 joined
<cl2> hello
```

* (terminal3) ctrl-c to kill cl2
* (terminal1) get a server crash with with `trio.Cancelled` errors all over the place
```
(triows) ~/g/triows (master)> hypercorn --worker-class trio --bind 127.0.0.1:8080 --reload server
[2021-07-31 09:46:53 +0930] [57362] [INFO] Running on http://127.0.0.1:8080 (CTRL + C to quit)
[2021-07-31 09:49:10,990] ERROR in app: Exception on websocket /ws
Traceback (most recent call last):
  File "/home/jtuckey/.pyenv/versions/3.9.6/envs/triows/lib/python3.9/site-packages/quart_trio/app.py", line 175, in handle_websocket
    return await self.full_dispatch_websocket(websocket_context)
  File "/home/jtuckey/.pyenv/versions/3.9.6/envs/triows/lib/python3.9/site-packages/quart_trio/app.py", line 197, in full_dispatch_websocket
    result = await self.handle_user_exception(error)
  File "/home/jtuckey/.pyenv/versions/3.9.6/envs/triows/lib/python3.9/site-packages/quart_trio/app.py", line 166, in handle_user_exception
    raise error
  File "/home/jtuckey/.pyenv/versions/3.9.6/envs/triows/lib/python3.9/site-packages/quart_trio/app.py", line 195, in full_dispatch_websocket
    result = await self.dispatch_websocket(websocket_context)
  File "/home/jtuckey/.pyenv/versions/3.9.6/envs/triows/lib/python3.9/site-packages/quart/app.py", line 1651, in dispatch_websocket
    return await self.ensure_async(handler)(**websocket_.view_args)
  File "/home/jtuckey/g/triows/server.py", line 104, in wsocket
    nursery.start_soon(receiving, u)
  File "/home/jtuckey/.pyenv/versions/3.9.6/envs/triows/lib/python3.9/site-packages/trio/_core/_run.py", line 815, in __aexit__
    raise combined_error_from_nursery
trio.MultiError: Cancelled(), Cancelled(), Cancelled()

Details of embedded exception 1:

  Traceback (most recent call last):
    File "/home/jtuckey/.pyenv/versions/3.9.6/envs/triows/lib/python3.9/site-packages/trio/_core/_run.py", line 1172, in raise_cancel
      raise Cancelled._create()
  trio.Cancelled: Cancelled

Details of embedded exception 2:

  Traceback (most recent call last):
    File "/home/jtuckey/g/triows/server.py", line 69, in receiving
      data = await websocket.receive_json()
    File "/home/jtuckey/.pyenv/versions/3.9.6/envs/triows/lib/python3.9/site-packages/quart/wrappers/websocket.py", line 68, in receive_json
      data = await self.receive()
    File "/home/jtuckey/.pyenv/versions/3.9.6/envs/triows/lib/python3.9/site-packages/quart/wrappers/websocket.py", line 57, in receive
      return await self._receive()
    File "/home/jtuckey/.pyenv/versions/3.9.6/envs/triows/lib/python3.9/site-packages/trio/_channel.py", line 314, in receive
      return await trio.lowlevel.wait_task_rescheduled(abort_fn)
    File "/home/jtuckey/.pyenv/versions/3.9.6/envs/triows/lib/python3.9/site-packages/trio/_core/_traps.py", line 166, in wait_task_rescheduled
      return (await _async_yield(WaitTaskRescheduled(abort_func))).unwrap()
    File "/home/jtuckey/.pyenv/versions/3.9.6/envs/triows/lib/python3.9/site-packages/outcome/_impl.py", line 138, in unwrap
      raise captured_error
    File "/home/jtuckey/.pyenv/versions/3.9.6/envs/triows/lib/python3.9/site-packages/trio/_core/_run.py", line 1172, in raise_cancel
      raise Cancelled._create()
  trio.Cancelled: Cancelled

Details of embedded exception 3:

  Traceback (most recent call last):
    File "/home/jtuckey/g/triows/server.py", line 54, in sending
      data = await u.queue_recv.receive()
    File "/home/jtuckey/.pyenv/versions/3.9.6/envs/triows/lib/python3.9/site-packages/trio/_channel.py", line 314, in receive
      return await trio.lowlevel.wait_task_rescheduled(abort_fn)
    File "/home/jtuckey/.pyenv/versions/3.9.6/envs/triows/lib/python3.9/site-packages/trio/_core/_traps.py", line 166, in wait_task_rescheduled
      return (await _async_yield(WaitTaskRescheduled(abort_func))).unwrap()
    File "/home/jtuckey/.pyenv/versions/3.9.6/envs/triows/lib/python3.9/site-packages/outcome/_impl.py", line 138, in unwrap
      raise captured_error
    File "/home/jtuckey/.pyenv/versions/3.9.6/envs/triows/lib/python3.9/site-packages/trio/_core/_run.py", line 1172, in raise_cancel
      raise Cancelled._create()
  trio.Cancelled: Cancelled

  During handling of the above exception, another exception occurred:

  Traceback (most recent call last):
    File "/home/jtuckey/g/triows/server.py", line 64, in sending
      await broadcast({'type': 'part', 'data': u.name})
    File "/home/jtuckey/g/triows/server.py", line 76, in broadcast
      await user.queue_send.send(message)
    File "/home/jtuckey/.pyenv/versions/3.9.6/envs/triows/lib/python3.9/site-packages/trio/_channel.py", line 159, in send
      await trio.lowlevel.checkpoint_if_cancelled()
    File "/home/jtuckey/.pyenv/versions/3.9.6/envs/triows/lib/python3.9/site-packages/trio/_core/_run.py", line 2361, in checkpoint_if_cancelled
      await _core.checkpoint()
    File "/home/jtuckey/.pyenv/versions/3.9.6/envs/triows/lib/python3.9/site-packages/trio/_core/_run.py", line 2339, in checkpoint
      await _core.wait_task_rescheduled(lambda _: _core.Abort.SUCCEEDED)
    File "/home/jtuckey/.pyenv/versions/3.9.6/envs/triows/lib/python3.9/site-packages/trio/_core/_traps.py", line 166, in wait_task_rescheduled
      return (await _async_yield(WaitTaskRescheduled(abort_func))).unwrap()
    File "/home/jtuckey/.pyenv/versions/3.9.6/envs/triows/lib/python3.9/site-packages/outcome/_impl.py", line 138, in unwrap
      raise captured_error
    File "/home/jtuckey/.pyenv/versions/3.9.6/envs/triows/lib/python3.9/site-packages/trio/_core/_run.py", line 1172, in raise_cancel
      raise Cancelled._create()
  trio.Cancelled: Cancelled
```
* (terminal2) no cl2 leave message is sent
